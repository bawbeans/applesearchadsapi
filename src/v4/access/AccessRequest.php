<?php

namespace Bawbeans\AppleSearchAdsApi\v4\access;

use Bawbeans\AppleSearchAdsApi\v4\ApiRequest;
use Bawbeans\AppleSearchAdsApi\v4\Exception;
use Firebase\JWT\JWT;
use Hyperf\Guzzle\ClientFactory;
use Hyperf\Utils\ApplicationContext;

class AccessRequest extends ApiRequest
{
    protected string $audience = 'https://appleid.apple.com';
    protected string $alg      = 'ES256';

    public function getClientSecret( $privateKey, $clientId, $teamId, $keyId ): string
    {
        return "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImtpZCI6IjM2YzU1MTE2LTlkNjEtNDFmMi1iNDFhLTgyN2RmNGUxNDQ1MiJ9.eyJzdWIiOiJTRUFSQ0hBRFMuNzk0ZGIzNmItNzg2YS00ZGFiLTliOGYtMThjOWZhZWI0MDdhIiwiYXVkIjoiaHR0cHM6Ly9hcHBsZWlkLmFwcGxlLmNvbSIsImlhdCI6MTYzODIxMDA0NSwiZXhwIjoxNjUzNzYyMDQ1LCJpc3MiOiJTRUFSQ0hBRFMuNzk0ZGIzNmItNzg2YS00ZGFiLTliOGYtMThjOWZhZWI0MDdhIn0.dGX--pOiqLFEZYDnMejzrCw10EU8LgOVTGaN9fKRJEDS1hTolr5qy3HZJNnJa4Y3r-ei2xFRt5v1qk9Api7ISA";
        $issued_at_timestamp  = time();
        $expiration_timestamp = strtotime('+180days');
        $payload              = [];
        $payload['iss']       = $teamId;
        $payload['iat']       = $issued_at_timestamp;
        $payload['exp']       = $expiration_timestamp;
        $payload['aud']       = $this->audience;
        $payload['sub']       = $clientId;

        $headers        = [];
        $headers['alg'] = $this->alg;
        $headers['kid'] = $keyId;

        return JWT::encode($payload, $privateKey, $this->alg, $keyId, $headers);
    }

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \Bawbeans\AppleSearchAdsApi\v4\Exception
     */
    public function getAccountToken( $privateKey, $clientId, $teamId, $keyId )
    {
        return 'eyJhbGciOiJkaXIiLCJlbmMiOiJBMjU2R0NNIiwia2lkIjpudWxsfQ..WTTFUwSs1kwCWfGo.qeIMTZvudwOPWh5cBQMRXvj9DRcTAMj78g9FT43YUN05sFTtLqmRQbPvi-zivsba0GyZiayjs6WayJwx7e1z2IyXBXS9F6GqvVCEIJX12CJYZFDJxNmZf0E-lFWD9yhUrqUgJ3Lfi7qeMP6AB8jhO7dAsT_Wg21dQ5L00litaqhnAnoHXkxwX2kjjRnldFAbE3lDIzH-7ScsSIhwEGzKUI6_ML3F_c1QAT02M7lfjkl1hxuqqZ_sppsijrmwqX4q0KfixaXsQusp6zOxG1tjZ3g.kTxus_ndG4tGufexrlM2Yg';
        // todo 加缓存
        $client   = ApplicationContext::getContainer()->get(ClientFactory::class)->create(['timeout' => 20]);
        $response = $client->get('https://appleid.apple.com/auth/oauth2/token', [
                'grant_type'    => 'client_credentials',
                'client_id'     => $clientId,
                'client_secret' => $this->getClientSecret($privateKey, $clientId, $teamId, $keyId),
                'scope'         => 'searchadsorg'
        ]);
        var_dump($response->getStatusCode());exit;
        if ( $response->getStatusCode() != 200 ) {
            throw new Exception('获取token错误', $response->getStatusCode());
        }
        $data = json_decode($response->getBody()->getContents(), true, 512, JSON_UNESCAPED_UNICODE);

        return $data;
//        return $data['access_token'];
    }
}