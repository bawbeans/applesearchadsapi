<?php

namespace Bawbeans\AppleSearchAdsApi\v4;

class ApiRequest extends BaseApi
{
    protected string $requestUrl;
    protected array  $response;

    /**
     * @param string $url url part after version(e.g. keywords/targeting/find)
     * @throws Exception
     */
    public function getUrl( string $url )
    {
        $url = ltrim($url, "/");
        if ( !$url ) {
            throw new Exception("Invalid url");
        }

        return $this->getBaseUrl() . "/" . trim($url);
    }
}