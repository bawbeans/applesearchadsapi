<?php

use Bawbeans\AppleSearchAdsApi\v4\report\ReportRequest;
use Bawbeans\AppleSearchAdsApi\v4\access\AccessRequest;
use Bawbeans\AppleSearchAdsApi\v4\Http;

require './vendor/autoload.php';

$header = [
        'Host: appleid.apple.com',
        'Content-Type: application/json',
        'X-AP-Context: orgId=2676300',
        'Authorization: Bearer eyJhbGciOiJkaXIiLCJlbmMiOiJBMjU2R0NNIiwia2lkIjpudWxsfQ..5s_poUj7GMSZzYB_.fciY8tm9EMYC2doUP0IKKtM-Qum9TFRcf0PgbB9ZvYDtX-yHUHidR5tWYiR600bn-YrYgAlykVvmBjhVEAf1Jka0JHC63W57-n7FOlPc7AO-3NS3ho2RC3OG9XFrXgR1CjD_KNphuNTbFwzZBuB1murnFg52kaRyaOWuI9WNsTOYvP7Ld4Fs6jyaWFWDd0dqXLTEF5Ww8wBqU3YudXXVmLHMTlXW7-m39TYr_V37o1vDti5d-FObTig983fATz4Lxfd4EkuZdArEwgCTyzKXmT4.q899arfN40TfRbuXd79jLg'
];
$json   = <<<JSON
{
    "startTime": "2021-11-30",
    "endTime": "2021-11-30",
    "selector": {
        "orderBy": [
            {
                "field": "countryOrRegion",
                "sortOrder": "ASCENDING"
            }
        ],
        "conditions": [
            {
                "field": "countriesOrRegions",
                "operator": "CONTAINS_ANY",
                "values": [
                    "US",
                    "GB"
                ]
            },
            {
                "field": "countryOrRegion",
                "operator": "IN",
                "values": [
                    "US"
                ]
            }
        ],
        "pagination": {
            "offset": 0,
            "limit": 1000
        }
    },
    "groupBy": [
        "countryOrRegion"
    ],
    "timeZone": "UTC",
    "returnRecordsWithNoMetrics": true,
    "returnRowTotals": true,
    "returnGrandTotals": true
}
JSON;
$option = json_decode($json, true);
$option = [];
$data   = Http::curl('https://api.searchads.apple.com/api/v4/reports/campaigns', $option, $header, 'POST', 10);
var_dump($data);
exit;

$clientId   = 'SEARCHADS.794db36b-786a-4dab-9b8f-18c9faeb407a';
$teamId     = 'SEARCHADS.794db36b-786a-4dab-9b8f-18c9faeb407a';
$keyId      = '36c55116-9d61-41f2-b41a-827df4e14452';
$orgid      = 2676300;
$privateKey = <<<KEY
-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIMcnZ9UQQEf8vDQsju+UU7+mE04+XXJY6V8hC9zE1eQioAoGCCqGSM49
AwEHoUQDQgAECfAQ5xeFWRWAQKvCu0IHJSVTjLZpTZfqJ4d6wW6DR42Mn6Nwxyuu
+aC8MkQTkaSfXQo6suwqlBhMVD5d9Jfz1w==
-----END EC PRIVATE KEY-----
KEY;

try {
    $param = ( new ReportRequest )
            ->setStartTime(date('Y-m-d'))
            ->setEndTime(date('Y-m-d'))
            ->setOrderBy([
                    ["field" => "countryOrRegion", "sortOrder" => "ASCENDING"]
            ])
            ->setCondition([])
            ->setGroupBy(['countryOrRegion'])
            ->campaign();
} catch ( \Bawbeans\AppleSearchAdsApi\v4\Exception $e ) {
    echo 1;
} catch ( \Psr\Container\NotFoundExceptionInterface $e ) {
    echo 2;
} catch ( \Psr\Container\ContainerExceptionInterface $e ) {
    echo 3;
}

var_dump($param);
