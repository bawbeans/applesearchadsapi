<?php

namespace Bawbeans\AppleSearchAdsApi\v4\campaigns;

use Bawbeans\AppleSearchAdsApi\v4\ApiRequest;
use Bawbeans\AppleSearchAdsApi\v4\Exception;

class CampaignRequest extends ApiRequest
{
    /**
     * @throws Exception
     */
    public function create( $param ): array
    {
        $this->setPost()->setUrl('campaigns')->setParam($param)->run();

        return $this->response;
    }

    /**
     * @throws Exception
     */
    public function find( array $conditions, array $pagination, array $orderBy ): array
    {
        $param               = [];
        $param['pagination'] = [
                'offset' => $this->offset,
                'limit'  => $this->limit,
        ];
        $param['orderBy']    = [
                ['field' => 'id', 'sortOrder' => 'ASCENDING']
        ];
        $param['conditions'] = [
                ['field' => 'countriesOrRegions', 'operator' => 'CONTAINS_ALL', 'values' => ['US', 'CA']]
        ];
        $this->setPost()->setUrl('campaigns/find')->setParam($param)->run();

        return $this->response;
    }

    /**
     * @throws Exception
     */
    public function get( $campaignId ): array
    {
        $this->setGet()->setUrl('campaigns/' . $campaignId)->run();

        return $this->response;
    }

    /**
     * @throws Exception
     */
    public function getAll( $limit = 20, $offset = 0 ): array
    {
        $param = ['limit' => $limit, 'offset' => $offset];

        $this->setGet()->setUrl('campaigns')->setParam($param)->run();

        return $this->response;
    }

    /**
     * @throws Exception
     */
    public function update( $campaignId, $param ): array
    {
        $param = [
                "clearGeoTargetingOnCountryOrRegionChange" => true,
                'campaign'                                 => $param
        ];

        $this->setPut()->setUrl('campaigns/' . $campaignId)->setParam($param)->run();

        return $this->response;
    }

    /**
     * 删除
     *
     * @throws Exception
     */
    public function delete( $campaignId ): array
    {
        $this->setDelete()->setUrl('campaigns/' . $campaignId)->run();

        return $this->response;
    }

}