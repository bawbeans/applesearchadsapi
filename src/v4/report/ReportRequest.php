<?php

namespace Bawbeans\AppleSearchAdsApi\v4\report;

use Bawbeans\AppleSearchAdsApi\v4\ApiRequest;
use Bawbeans\AppleSearchAdsApi\v4\Exception;
use Bawbeans\AppleSearchAdsApi\v4\Http;

class ReportRequest extends ApiRequest
{
    protected string $startTime;
    protected string $endTime;
    protected array  $orderBy                    = [];
    protected array  $conditions                 = [];
    protected array  $groupBy                    = [];
    protected string $timeZone                   = 'ORTZ';
    protected bool   $returnRecordsWithNoMetrics = true;
    protected bool   $returnRowTotals            = true;
    protected bool   $returnGrandTotals          = false;
    protected int    $limit                      = 1000;
    protected int    $offset                     = 0;
    protected array  $headers                    = [];
    protected string $granularity                = 'DAILY';
    protected int    $orgId                      = 0;
    protected string $accountToken               = '';

    public function setOrgId( $orgId )
    {
        $this->orgId = $orgId;

        return $this;
    }

    public function setAccountToken( $token )
    {
        $this->accountToken = $token;

        return $this;
    }

    public function setLimit( $limit )
    {
        $this->limit = $limit;

        return $this;
    }

    public function setOffset( $offset )
    {
        $this->offset = $offset;

        return $this;
    }

    public function setStartTime( string $date )
    {
        $this->startTime = $date;

        return $this;
    }

    public function setEndTime( string $date )
    {
        $this->endTime = $date;

        return $this;
    }

    public function setOrderBy( ...$orderBy )
    {
        $this->orderBy = $orderBy;

        return $this;
    }

    public function setGranularity( $granularity )
    {
        $this->granularity = $granularity;

        return $this;
    }

    public function setCondition( ...$conditions )
    {
        $this->conditions = $conditions;

        return $this;
    }

    public function setReturnRowTotals( $bool = true )
    {
        $this->returnRowTotals = $bool;

        return $this;
    }

    public function setReturnGrandTotals( $bool = true )
    {
        $this->returnGrandTotals = $bool;

        return $this;
    }

    public function setReturnRecordsWithNoMetrics( $bool = true )
    {
        $this->returnRecordsWithNoMetrics = $bool;

        return $this;
    }

    public function setTimeZone( $timeZone = 'UTC' )
    {
        $this->timeZone = $timeZone;

        return $this;
    }

    public function setGroupBy( ...$groupBy )
    {
        $this->groupBy = $groupBy;

        return $this;
    }

    public function getParam(): array
    {
        return [
                'startTime'                  => $this->startTime,
                'endTime'                    => $this->endTime,
                'selector'                   => [
                        'orderBy'    => $this->orderBy,
                        'conditions' => $this->conditions,
                        'pagination' => [
                                'offset' => $this->offset,
                                'limit'  => $this->limit,
                        ]
                ],
                'groupBy'                    => $this->groupBy,
                'timeZone'                   => $this->timeZone,
                'returnRecordsWithNoMetrics' => $this->returnRecordsWithNoMetrics,
                'returnRowTotals'            => $this->returnRowTotals,
                'returnGrandTotals'          => $this->returnGrandTotals,
        ];
    }

    /**
     * @throws Exception
     */
    public function campaign(): array
    {
        $url    = $this->getUrl('reports/campaigns');
        $param  = $this->getParam();
        $header = [
                'X-AP-Context: orgId=' . $this->orgId,
                'Authorization: Bearer ' . $this->accountToken,
                'Content-Type: application/json'
        ];
        $client = Http::init()->setUrl($url)
                ->setBody($param)
                ->setTimeout(20)
                ->setHeader($header)
                ->request();

        return json_decode($client->getResponse(), true);
    }

    /**
     * @throws Exception
     */
    public function adGroup( $campaigns ): array
    {
        $url    = $this->getUrl('reports/campaigns/' . $campaigns . '/adgroups');
        $header = [
                'X-AP-Context: orgId=' . $this->orgId,
                'Authorization: Bearer ' . $this->accountToken,
                'Content-Type: application/json'
        ];
        $param  = $this->getParam();
        $client = Http::init()->setUrl($url)
                ->setBody($param)
                ->setTimeout(20)
                ->setHeader($header)
                ->request();

        return json_decode($client->getResponse(), true);
    }

    /**
     * @throws Exception
     */
    public function keyword( $campaigns ): array
    {
        $url    = $this->getUrl('reports/campaigns/' . $campaigns . '/keywords');
        $header = [
                'X-AP-Context: orgId=' . $this->orgId,
                'Authorization: Bearer ' . $this->accountToken,
                'Content-Type: application/json'
        ];
        $param  = $this->getParam();
        $client = Http::init()->setUrl($url)
                ->setBody($param)
                ->setTimeout(20)
                ->setHeader($header)
                ->request();

        return json_decode($client->getResponse(), true);
    }

    /**
     *
     */
    public function searchTerm(): array
    {
        return $this->response;
    }

    /**
     *
     */
    public function creativeSet(): array
    {
        return $this->response;
    }
}