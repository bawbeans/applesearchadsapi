<?php

namespace Bawbeans\AppleSearchAdsApi\v4;

use Bawbeans\AppleSearchAdsApi\v4\Exception;

class BaseApi
{
    protected string $baseUrl = 'https://api.searchads.apple.com/api/';
    protected string $version = 'v4';

    /**
     * @return string
     */
    protected function getBaseUrl(): string
    {
        return $this->baseUrl . $this->version;
    }
}